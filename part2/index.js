const http = require('http');

const port = 8000;

const server = http.createServer((request, response) => {

	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("login route");
	}
	if (request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("register route");
	}
	if (request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("homepage route");
	}
	if (request.url == '/courses'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("courses route");
	}
	if (request.url == '/usersProfile'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("usersProfile route");
	}
	if (request.url == '/'){
		response.writeHead(403, {'Content-Type': 'text/plain'});
		response.end("forbidden access");
	}

});

server.listen(port);

console.log(`Server is now running at localhost: ${port}.`);
